# Generate self-signed SSL Cert

## How-to

```

vim openssl.cnf

openssl genrsa -out server.key 4096

openssl req -new -sha256 \
    -out server.csr \
    -key server.key \
    -config openssl.cnf

# check csr
openssl req -text -noout -in server.csr

# Generate the certificate
openssl x509 -req \
    -days 3650 \
    -in server.csr \
    -signkey server.key \
    -out server.crt \
    -extensions req_ext \
    -extfile openssl.cnf

# Add the certificate to keychain and trust it:
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain server.crt

```