DOCKER_COMPOSE  ?= docker-compose

build:
	$(DOCKER_COMPOSE) build --pull
up:
	$(DOCKER_COMPOSE) up -d
	@sleep 1
	$(DOCKER_COMPOSE) ps
clean:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down -v --remove-orphans

bash-root:
	$(DOCKER_COMPOSE) exec php bash

bash:
	$(DOCKER_COMPOSE) exec --user=application php bash

open:
	open https://docker.local:$(shell $(DOCKER_COMPOSE) port php 443 | sed 's/[0-9.]*://')