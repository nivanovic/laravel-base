FROM webdevops/php-nginx:7.2

LABEL maintainer="nivanovic85@gmail.com"

# Docker settings
ENV WEB_ALIAS_DOMAIN=*.local
ENV WEB_SERVER_NAME=docker.local
COPY image-files/ /

# Working directory
WORKDIR /app

# Set document root
ENV WEB_DOCUMENT_ROOT /app/public

# Application source-code
COPY ./app ./app/
COPY ./bootstrap ./bootstrap/
COPY ./config ./config/
COPY ./database ./database/
COPY ./public ./public/
COPY ./resources ./resources/
COPY ./routes ./routes/
COPY ./storage ./storage/
COPY ./artisan ./artisan
COPY ./.env.example ./.env
COPY ./vendor ./vendor

# Install composer packages
COPY composer.* ./
RUN composer install --prefer-dist --optimize-autoloader